# Get First Tweet ID

Returns the ID of the first tweet (as made available by the Twitter API) for each user included in a list of Twitter usernames.